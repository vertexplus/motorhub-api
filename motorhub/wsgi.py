"""
WSGI config for motorhub project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.2/howto/deployment/wsgi/
"""

import os

from django.core.wsgi import get_wsgi_application

dbfile = 'motorhub.settings.production'

f = open(os.path.dirname(os.path.dirname(os.path.abspath(__file__)))+"/.git/HEAD", "r")
data = f.read()
if 'ref: refs/heads/development' in data:
	dbfile = 'motorhub.settings.testing'
if 'ref: refs/heads/uat' in data:
	dbfile = 'motorhub.settings.uat'
f.close()

os.environ.setdefault('DJANGO_SETTINGS_MODULE',dbfile)

application = get_wsgi_application()