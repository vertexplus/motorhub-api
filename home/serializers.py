from rest_framework import serializers
from .models import Vehicle, FutureVehicle, User, WishToSell, ShopInsurance, PurchaseVehicle

class EditProfileSerializers(serializers.ModelSerializer):
    u_image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = User
        fields = ('email', 'u_image', 'suffix', 'first_name', 'last_name', 'mobile_code', 'mobile', 'dob', 'gender', 'marital_status', 'address', 'address2', 'zip', 'residence', 'licensed_drive_before_41', 'affiliated_with', 'employment', 'occuption', 'job_title')
    

class AddVehicleSerializer(serializers.ModelSerializer):
    window_sticker = serializers.ImageField(allow_empty_file=True, required=False)
    insurance_card = serializers.ImageField(allow_empty_file=True, required=False)
    service_date = serializers.ListSerializer(child=serializers.DateField(), required=False)
    service_image = serializers.ListField(allow_empty=False, child=serializers.ImageField(), required=False)
    vehicle_image = serializers.ListField(allow_empty=False, child=serializers.ImageField(), required=False)


    class Meta:
        model = Vehicle
        fields = ('id', 'vin', 'make', 'marked_for_sale', 'model', 'model_year', 'series', 'trim', 'window_sticker', 'insurance_card', 'service_date', 'service_image', 'vehicle_image', 'vehicle_type', 'primery_use', 'Commute', 'Commute_choice', 'commute_one_way', 'mileage', 'parking_location', 'drivers_under_25', 'accidents_past_3_years', 'claims_past_3_years', 'speed_tickets_past_3_years', 'drivers_in_the_household', 'iq_score')
        # exclude = ('service_records', 'vehicle_images')
    

class EditAddVehicleSerializers(serializers.ModelSerializer):
    images = serializers.ImageField(allow_empty_file=True, required=False)
    insurance_card = serializers.ImageField(allow_empty_file=True, required=False)
    service_date = serializers.ListSerializer(child=serializers.DateField(), required=False)
    service_image = serializers.ListField(allow_empty=False, child=serializers.ImageField(), required=False)
    vehicle_image = serializers.ListField(allow_empty=False, child=serializers.ImageField(), required=False)

    class Meta:
        model = Vehicle
        fields = ('id', 'vin', 'make', 'marked_for_sale', 'model', 'model_year', 'series', 'trim', 'images', 'insurance_card', 'service_date', 'service_image', 'vehicle_image', 'vehicle_type', 'primery_use', 'Commute', 'Commute_choice', 'commute_one_way', 'mileage', 'parking_location', 'drivers_under_25', 'accidents_past_3_years', 'claims_past_3_years', 'speed_tickets_past_3_years', 'drivers_in_the_household', 'iq_score')
    


class FutureVehicleSerializers(serializers.ModelSerializer):
    # f_v_image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = FutureVehicle
        fields = ('id', 'vehicle_condition', 'make', 'model', 'price_from', 'price_to', 'manufacture_year_from', 'manufacture_year_to', 'zip', 'distance', 'fule_type', 'iq_score')

class EditFutureVehicleSerializers(serializers.ModelSerializer):
    # f_v_image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = FutureVehicle
        fields = ('id', 'vehicle_condition', 'make', 'model', 'price_from', 'price_to', 'manufacture_year_from', 'manufacture_year_to', 'zip', 'distance', 'fule_type', 'iq_score')


class WishToSellSerializers(serializers.ModelSerializer):
    # image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = WishToSell
        fields = ('id', 'asking_price', 'mileage_vehicle')

class ShopInsuranceSerializer(serializers.ModelSerializer):
    # image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = ShopInsurance
        fields = ('vehicles', 'How_Much_you_Paying', 'How_Much_want_Pay', 'Current_Insurer', 'all_Vehicles_listed', 'vehicle_type', 'primery_use', 'Commute', 'Commute_choice', 'commute_one_way', 'mileage', 'parking_location', 'drivers_under_25', 'accidents_past_3_years', 'claims_past_3_years', 'speed_tickets_past_3_years', 'drivers_in_the_household')

class PurchaseVehicleSerializers(serializers.ModelSerializer):
    # image = serializers.ImageField(allow_empty_file=True, required=False)

    class Meta:
        model = PurchaseVehicle
        fields = ('id')