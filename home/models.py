from django.db import models
from django.contrib.sites.models import Site
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import User
import secrets
from django.db.models.signals import post_save
from django.dispatch import receiver
from home.utils import vin_detail

status_choice = (("Active","Active"),("Inactive","Inactive"),("Delete","Delete"))
details_choice = (("Dealer","Dealer"),("Customer","Customer"))
suffix_choice = (("Mr", "Mr"), ("Mrs", "Mrs"), ("Miss", "Miss"))
dstatus = (("Active","Active"),("Inactive","Inactive"),("Delete","Delete"),("Reject","Reject"))
role = (('Admin', 'Admin'), ('Company', 'Company'), ('Dealer', 'Dealer'),('Customer', 'Customer'),)
dealer_choices = (('Admin', 'Admin'), ('Salesman', 'Salesman'), ('Agent','Agent'))
quotation_choice = (("1", "Top 1"), ("2", "Top 2"), ("3", "Top 3"), ("4", "Top 4"), ("5", "Top 5"),("10", "Top 10"), ("Manual", "Manual"), ("All", "All"))
residence_type = (('Owner', 'Owner'), ('Rental', 'Rental'))
parking_location_choice = (('Garage', 'Garage'), ('Open', 'Open'),('Street','Street'))
primary_use_choice = (('Business', 'Business'), ('Personal', 'Personal'),('Commute (to work or school)','Commute (to work or school)'),('Pleasure','Pleasure'),('Business (sales, calls , etc.)','Business (sales, calls , etc.)'))
gender_choice = (('Select','Select'),('Male','Male'),('Female','Female'),('Transgender','Transgender'), ('Other', 'Other'))
marital_choice = (('Select','Select'),('Married','Married'),('Unmarried','Unmarried'))
licensed_choice  = (('Yes','Yes'),('No','No'))
affiliated_choice = (('Military','Military'),('State/Local Goverment','State/Local Goverment'),('Federal Government/Postal Services','Federal Government/Postal Services'),('Dose Not Apply','Dose Not Apply'))
employment_choice = (('Full Time','Full Time'),('Part Time','Part Time'))
# job_choice = (('Option 1','Option 1'),('Option 2','Option 2'),('Option 3','Option 3'))
vehicle_type_choice = (('Owned','Owned'),('Financed','Financed'),('Leased','Leased'))
commute_option_choice = (('One','One'),('Two','Two'),('Three','Three'))
mileage_choice = (('6001-7500','6001-7500'),('7501-9000','7501-9000'),('9001-12000','9001-12000'),('12001-15000','12001-15000'),('15001-20000','15001-20000'),('20000+','20000+'))
price_from_choice = (('8,00,000','8,00,000'),('10,00,000','10,00,000'),('15,00,000','15,00,000'),('20,00,000','20,00,000'))
price_to_choice = (('10,00,000','10,00,000'),('15,00,000','15,00,000'),('20,00,000','20,00,000'),('25,00,000','25,00,000'))
manufacture_year_from_choice = (('2015','2015'),('2016','2016'),('2017','2017'),('2018','2018'),('2019','2019'),('2020','2020'),('2021','2021'),('2022','2022'),('2023','2023'))
manufacture_year_to_choice = (('2015','2015'),('2016','2016'),('2017','2017'),('2018','2018'),('2019','2019'),('2020','2020'),('2021','2021'),('2022','2022'),('2023','2023'))
drivers_household_choice = (('One', 'One'), ('Two','Two'), ('three', 'three'))
vehicle_condition_choice = (('New', 'New'), ('Used', 'Used'))
fule_type_choice = (('Gas', 'Gas'), ('Diesel', 'Diesel'), ('Electric', 'Electric'), ('Hybrid', 'Hybrid'), ('Plug-In Hybrid', 'Plug-In Hybrid'))
Current_Insurer_choice = (('Insurer 1', 'Insurer 1'), ('Insurer 2', 'Insurer 2'))
make_choice = (("Macktrucks", "Macktrucks"), ("Volvotrucks", "Volvotrucks"), ("Toyota", "Toyota"), ("Chevrolet", "Chevrolet"), ("Ford", "Ford"), ("Honda", "Honda"), ("Nissan", "Nissa"), ("Hyundai", "Hyundai"), ("Volkswagen", "Volkswagen"), ("Volvo", "Volvo"), ("Hyundai", "Hyundai"), ("Mercedes", "Mercedes"), ("BMW", "BMW"), ("Fiat", "Fiat"), ("Acura", "Acura"), ("Jaguar", "Jaguar"), ("Land Rover", "Land Rover"), ("Mazda", "Mazda"), ("Subaru", "Subaru"), ("Mini", "Mini"), ("Mitsubishi", "Mitsubishi"), ("Buick", "Buick"), ("Cadillac", "Cadillac"), ("Lexus", "Lexus"), ("Chrysler", "Chrysler"), ("Lincoln", "Lincoln"), ("Audi", "Audi"), ("Jeep", "Jeep"), ("AlfaRomeo", "AlfaRomeo"), ("Dodge", "Dodge"), ("Kia", "Kia"), ("Infiniti", "Infiniti"), ("Genesis", "Genesis"), ("Porsche", "Porsche"), ("Smart", "Smart"), ("Tesla", "Tesla"), ("International", "International"), ("Isuzucv", "Isuzucv"), ("Demanddetroit", "Demanddetroit"), ("Freightliner", "Freightliner"), ("Selectrucks", "Selectrucks"), ("Thomasbuilt", "Thomasbuilt"), ("Westernstar", "Westernstar"), ("Fordcommercial", "Fordcommercial"), ("Paccar", "Paccar"))

class User(AbstractUser):
    site = models.ForeignKey(Site, on_delete=models.CASCADE)
    suffix = models.CharField(max_length=20, choices=suffix_choice, blank=True, null=True)
    first_name = models.CharField(max_length=100,blank=True, null=True) 
    last_name = models.CharField(max_length=100,blank=True, null=True) 
    u_image = models.ImageField(upload_to='user/profile/', null=True, blank=True)
    email = models.EmailField(blank=False, unique=True, default='username')
    mobile_code = models.CharField(max_length=15, blank=True, null=True)
    mobile = models.CharField(max_length=15,blank=True, null=True)
    dob = models.DateField(null=True,blank=True)
    gender = models.CharField(max_length=20, choices=gender_choice,blank=True, null=True)
    marital_status = models.CharField(max_length=20, choices=marital_choice,blank=True, null=True)
    address = models.CharField(max_length=100,blank=True, null=True) 
    address2 = models.CharField(max_length=100,blank=True, null=True)
    zip = models.CharField(max_length=100,blank=True, null=True)
    residence = models.CharField(max_length=20, choices=residence_type,blank=True, null=True)
    licensed_drive_before_41 = models.CharField(max_length=20, choices=licensed_choice,blank=True, null=True)
    affiliated_with = models.CharField(max_length=100, choices=affiliated_choice,blank=True, null=True)
    employment = models.CharField(max_length=20, choices=employment_choice,blank=True, null=True)
    occuption = models.CharField(max_length=100,blank=True, null=True)
    job_title = models.CharField(max_length=100,blank=True, null=True)
    otp = models.CharField(max_length=6, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username', 'site_id']

    def __str__(self):
        return self.email
    

class Vehicle(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vin = models.CharField(max_length=100)
    make = models.CharField(max_length=100, choices=make_choice)
    marked_for_sale = models.BooleanField(default=False) 
    model = models.CharField(max_length=200)
    model_year = models.CharField(max_length=200)
    series = models.CharField(max_length=200)
    trim = models.CharField(max_length=200)
    insurance_card = models.ImageField(upload_to='vehicle/', null=True, blank=True) 
    window_sticker = models.ImageField(upload_to='vehicle/', null=True, blank=True)
    vin_layout = models.TextField(null=True, blank=True)
    vehicle_type = models.CharField(max_length=100, choices=vehicle_type_choice,blank=True, null=True)
    primery_use = models.CharField(max_length=100, choices=primary_use_choice,blank=True, null=True) 
    Commute = models.CharField(max_length=100, blank=True, null=True) 
    Commute_choice = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True) 
    commute_one_way = models.CharField(max_length=100, null=True, blank=True) 
    mileage = models.CharField(max_length=200, null=True)
    parking_location = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    drivers_under_25 = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    accidents_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    claims_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    speed_tickets_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    drivers_in_the_household = models.CharField(max_length=100, choices=drivers_household_choice,blank=True, null=True)
    additional_driver = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    iq_score = models.CharField(max_length=200, default=0, null=True)
    cps = models.IntegerField(default=0)
    vin_decode = models.JSONField(default=list)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

    def __str__(self):
        return self.vin

class VehicleImage(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    vehicle_image = models.ImageField(upload_to='vehicle/vehicle_image/', null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

class ServiceRecord(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    service_date = models.DateField()
    service_image = models.ImageField(upload_to='vehicle/service_records/', null=True, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

class FutureVehicle(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    # site = models.CharField(max_length=100, default='default_value')
    f_v_image = models.ImageField(upload_to='user/profile/', null=True, blank=True)
    vehicle_condition = models.CharField(max_length=100, choices=vehicle_condition_choice,blank=True, null=True)
    make = models.CharField(max_length=200, choices=make_choice, blank=True, null=True)
    model = models.CharField(max_length=200)
    price_from = models.CharField(max_length=100, choices=price_from_choice, null=True, blank=True)
    price_to = models.CharField(max_length=100, choices=price_to_choice, null=True, blank=True)
    manufacture_year_from = models.CharField(max_length=100, choices=manufacture_year_from_choice, null=True, blank=True)
    manufacture_year_to = models.CharField(max_length=100, choices=manufacture_year_to_choice, null=True, blank=True)
    zip = models.CharField(max_length=200)
    distance = models.CharField(max_length=200) 
    fule_type = models.CharField(max_length=100, choices=fule_type_choice,blank=True, null=True)
    iq_score = models.CharField(max_length=200, choices=commute_option_choice, default=0, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

class WishToSell(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    # site = models.CharField(max_length=100, default='default_value')
    # vehicles = models.ForeignKey('Vehicle', on_delete=models.CASCADE)
    asking_price = models.IntegerField(null=True, blank=True) 
    mileage_vehicle = models.IntegerField(null=True, blank=True) 
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')

class ShopInsurance(models.Model):
    # user = models.ForeignKey(User, on_delete=models.CASCADE)
    # site = models.CharField(max_length=100, default='default_value')
    vehicles = models.ForeignKey(Vehicle, on_delete=models.CASCADE, null=True, blank=True)
    How_Much_you_Paying = models.IntegerField(null=True, blank=True) 
    How_Much_want_Pay = models.IntegerField(null=True, blank=True) 
    Current_Insurer = models.CharField(max_length=100, choices=Current_Insurer_choice, blank=True, null=True)
    all_Vehicles_listed  = models.BooleanField(default=False) 
    vehicle_type = models.CharField(max_length=100, choices=vehicle_type_choice,blank=True, null=True)
    primery_use = models.CharField(max_length=100, choices=primary_use_choice,blank=True, null=True) 
    Commute = models.CharField(max_length=100, blank=True, null=True) 
    Commute_choice = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True) 
    commute_one_way = models.CharField(max_length=100, null=True, blank=True) 
    mileage = models.CharField(max_length=200, null=True)
    parking_location = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    drivers_under_25 = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    accidents_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    claims_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    speed_tickets_past_3_years = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    drivers_in_the_household = models.CharField(max_length=100, choices=drivers_household_choice,blank=True, null=True)
    additional_driver = models.CharField(max_length=100, choices=commute_option_choice,blank=True, null=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)

class PurchaseVehicle(models.Model):
    vehicles = models.ForeignKey('Vehicle', on_delete=models.CASCADE)
    FutureVehicle = models.ForeignKey('FutureVehicle', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    utimestamp = models.DateTimeField(auto_now=True)
    track = models.TextField(blank=True, editable=False)
    utrack = models.TextField(blank=True, editable=False)
    status = models.CharField(max_length=20, choices=status_choice, default='Active')
    details = models.CharField(max_length=20, choices=details_choice, default='Customer')

@receiver(post_save, sender=Vehicle, dispatch_uid="vehicle_signal")
def vehicle_signal(sender, instance, created, **kwargs):
    print('Signal triggered')
    print(instance, 'Vehicle')
    if created:
        try:
            result = vin_detail(instance.vin)
            instance.cps = int(result['Vehicle_IQ_Score'])
            instance.vin_decode = result
            print(result['original_layout']['images'])
            if 'images' in result['original_layout']:
                instance.vin_layout = result['original_layout']['images'][0]
            instance.save()
        except Exception as e:
            print(f'Error in signal: {e}')

@receiver(post_save, sender=VehicleImage, dispatch_uid="vehicle_image_signal")
def vehicle_image_signal(sender, instance, created, **kwargs):
    print('vehicle image Signal triggered')
    print(instance, 'insta vehicle image')
    vehicle = Vehicle.objects.filter(id=instance.vehicle.id).first()
    print(vehicle, '6565665656565')
    print(vehicle, 'vehicle')
    print('vin_layout')
    vehicle.vin_layout = "/media/"+str(instance.vehicle_image)
    vehicle.save()
