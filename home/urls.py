from django.urls import path
from . import views
from django.urls import path
from django.contrib.auth import views as auth_views 

from .views import (
    homeview,
    AddFutureVehicleView,
    AddVehicleView,
    edit_profile_view,
    garage_offer_view,
    home_one_view,
    home_two_view,
    login_view,
    logout_user,
    my_profile_view,
    enter_otp,
    F_password_view,
    send_otp, 
    password_reset,
    WishToSellView, 
    Purchase_Vehicle_view,
    shop_insurance_view,
    DeleteVehicleView,
    edit_add_vehicle_view, 
    DeleteFutureVehicleView, 
    edit_future_vehicle_view,
)


urlpatterns = [
    path("", homeview, name="home"),
    path("add-future-vehicle", AddFutureVehicleView, name="add-future-vehicle"),
    path("edit_future_vehicle/<int:id>/", edit_future_vehicle_view, name="edit_future_vehicle"),
    path("delete-future-vehicle/<int:id>/", DeleteFutureVehicleView, name="delete-future-vehicle"),
    path("add-vehicle/", AddVehicleView, name="add-vehicle"),
    path("edit_add_vehicle/<int:id>/", edit_add_vehicle_view, name="edit_add_vehicle"),
    path("delete-vehicle/<int:id>/", DeleteVehicleView, name="delete-vehicle"),
    path('wish-sell/', WishToSellView, name='wish-to-sell'),
    path("edit-profile/<int:id>/", edit_profile_view, name="edit_profile"),
    path("garage-offer/", garage_offer_view, name="garage_offer"),
    path("shop-insurance/", shop_insurance_view, name="shop_Insurance"),
    path("purchase-vehicle/", Purchase_Vehicle_view, name="Purchase_Vehicle"),
    path("login/", login_view, name="login"),
    path("logout/", logout_user, name="logout"),
    path("my-profile/", my_profile_view, name="my-profile"),
    path('F_password_view', F_password_view, name="F_password_view"),
    path('send-otp', send_otp, name="send_otp"),
    path('enter-otp', enter_otp, name="enter_otp"),
    path('password-reset', password_reset, name="password_reset"),
   


]


