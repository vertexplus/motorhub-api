//Tooltips
const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]')
const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl))

//multiple slide
$('.garage-slide').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                dots: true
            }
        }
    ]
});
//Mobile View click open side menu
if ($(window).width() > 767) {
    $('.offcanvas-details .offerside-tab').click(function () {
        $('.offerside-tab').css({ 'border-color': '#d2d2d2', 'outline': '0', 'background': '#0081C6' })
        $(this).parents('.offcanvas-left').next('.tab-content').css('display', 'block')
        $('.offerside-tab.active').css({ 'border-color': '#0081C6', 'outline': '1px solid #0081C6', 'background': '#F3F3F3' })
    })
    //Close
    $('.btn-close, .m-detail-close').click(function () {
        $('.offcanvas-right').css('display', 'none');
        $('.offerside-tab').css({ 'border-color': '#d2d2d2', 'outline': '0', 'background': '#fff' })
    })
}
//Offcanvase in mobile
if ($(window).width() < 767) {
    $('.offcanvas-details .offerside-tab').click(function () {
        $(this).parents('.offcanvas-left').next('.offcanvas-right').css('transform', 'translateX(0)')
    })
}
$('.m-detail-back, .btn-close').click(function () {
    $(this).parent('.offcanvas-right').css('transform', 'translateX(-100%)')
})
//Nav Items Active
$(document).ready(function () {
    $(window).scroll(function () {
        var Scroll = $(window).scrollTop() + 1,
            SectionOneOffset = $('#vehicles').offset().top,
            SectionTwoOffset = $('#future-vehicle').offset().top;
        if (Scroll >= SectionOneOffset) {
            $(".menu-item-1").addClass("active");
        } else {
            $(".menu-item-1").removeClass("active");
        }
        if (Scroll >= SectionTwoOffset) {
            $(".menu-item-2").addClass("active");
            $(".menu-item-1").removeClass("active");
        } else {
            $(".menu-item-2").removeClass("active");
        }
    });
});
//Range Slider
const allRanges = document.querySelectorAll(".iqscore");
allRanges.forEach(wrap => {
    const range = wrap.querySelector(".slider");
    const bubble = wrap.querySelector(".rangevalue");

    range.addEventListener("input", () => {
        setBubble(range, bubble);
    });
    setBubble(range, bubble);
});

function setBubble(range, bubble) {
    const val = range.value;
    const min = range.min ? range.min : 0;
    const max = range.max ? range.max : 40000;
    const newVal = Number(((val - min) * 100) / (max - min));
    bubble.innerHTML = val;

    // Sorta magic numbers based on size of the native UI thumb
    bubble.style.left = `calc(${newVal}% + (${8 - newVal * 0.15}px))`;
}


