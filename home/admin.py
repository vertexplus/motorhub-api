from django.contrib import admin
from .models import User, Vehicle, FutureVehicle, VehicleImage, ServiceRecord, WishToSell, ShopInsurance, PurchaseVehicle


@admin.register(User)
class webuser(admin.ModelAdmin):
    list_display = ['username', 'email', 'address']
    search_fields = ['username']

@admin.register(Vehicle)
class webvehicle(admin.ModelAdmin):
    list_display = ['vin', 'model', 'model_year', 'series']
    search_fields = ['model']

@admin.register(FutureVehicle)
class webFuturevehicle(admin.ModelAdmin):
    list_display = ['vehicle_condition', 'model', 'price_from', 'make']
    search_fields = ['vehicle_condition']

@admin.register(VehicleImage)
class VehicleImageAdmin(admin.ModelAdmin):
    list_display = ['id', 'vehicle_image']

@admin.register(ServiceRecord)
class ServiceRecordAdmin(admin.ModelAdmin):
    list_display = ('service_date', 'service_image') 
    
@admin.register(WishToSell)
class WishtosellAdmin(admin.ModelAdmin):
    list_display = ('mileage_vehicle', 'asking_price') 

@admin.register(ShopInsurance)
class shop_InsuranceAdmin(admin.ModelAdmin):
    list_display = ('Current_Insurer', 'How_Much_want_Pay') 

@admin.register(PurchaseVehicle)
class PurchaseVehicleAdmin(admin.ModelAdmin):
    list_display = ('details', 'vehicles', 'status') 