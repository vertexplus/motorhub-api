from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login 
from home.models import User, Vehicle, FutureVehicle, VehicleImage, ServiceRecord, PurchaseVehicle
from rest_framework import status
from .serializers import FutureVehicleSerializers, EditProfileSerializers, WishToSellSerializers, ShopInsuranceSerializer, PurchaseVehicleSerializers, EditAddVehicleSerializers, EditFutureVehicleSerializers
import random
import time
from django.core.mail import EmailMessage
from django.shortcuts import render, redirect
from .models import User
from django.contrib import messages
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.conf import settings

# ---------------------------------------------------------------LOGIN -----------------------------------------------------
def login_view(request):
    error_message = None
    if request.method == "POST":
        email = request.POST.get("email")
        password = request.POST.get("password")

        # if email is None or password is None:
        if not email:
            error_message = "Email Is invalid "
        elif password:
            error_message = "Password is incorrect "
        else:
            messages.success(request, 'Password is changed successfully')

            return redirect("login.html")

        user = authenticate(request=request, email=email, password=password)
        if user:
            login(request, user)
            return redirect("home")
        if not error_message:
                    return redirect("login")

    return render(request, "login.html", {'error_message': error_message})

# ------------------------------------------------------------------LOGOUT-----------------------------------------------------
def logout_user(request):
    logout(request)
    messages.success(request, 'You have been logged out successfully.')
    return redirect('home')

# ---------------------------------------------------------------PROFILE -------------------------------------------------------------
@login_required
def my_profile_view(request):
    profile = request.user
    return render(request, "my-profile.html", {'profile': profile})

# -----------------------------------------------------------------EDIT-PROFILE -------------------------------------------------------
@login_required
def edit_profile_view(request, id):
    user_profile = User.objects.get(id=id)

    if request.method == 'POST':
        data_dict1 = request.POST.copy()
        data_dict1.update(request.FILES)

        serializer = EditProfileSerializers(instance=user_profile, data=data_dict1, partial=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()

            return redirect('my-profile')
        else:
            return render(request, 'edit-profile.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'edit-profile.html', {'user_profile': user_profile})

# ---------------------------------------------------------------------------Add Vehicle -----------------------------------------------------------

@login_required
def AddVehicleView(request):
    if request.method == 'POST':
        valid_fields = {'vin', 'site', 'make', 'model', 'model_year', 'series', 'trim', 'vehicle_type', 'primery_use', 'Commute', 'Commute_choice', 'commute_one_way', 'mileage', 'parking_location', 'drivers_under_25', 'accidents_past_3_years', 'claims_past_3_years', 'speed_tickets_past_3_years', 'drivers_in_the_household', 'iq_score'}
        data_dict = {}
        mandatoryFields = ['vin','make','model','model_year','series',]
        errorMessage = {}
        for key in valid_fields:
            if key in request.POST:
                if key in mandatoryFields and not request.POST[key]:
                    errorMessage[key] = f'{key} is required'
                data_dict[key] = request.POST[key]
        if len(errorMessage.keys()) > 0:
            return render(request, 'add-vehicle.html', {'errorMessage': errorMessage})

        for key in ['window_sticker', 'insurance_card']:
            if key in request.FILES:
                data_dict[key] = request.FILES[key]

        vin = data_dict.pop('vin')
        print(request.user, 'login userrrrrrr')
        vehicle = Vehicle.objects.create(user = request.user, vin=vin, **data_dict)
        service_dates = request.POST.getlist('service_date')
        service_images = request.FILES.getlist('service_image')
        vehicle_images = request.FILES.getlist('vehicle_image')

        for service_date, service_image in zip(service_dates, service_images):
            ServiceRecord.objects.create(vehicle=vehicle, service_date=service_date, service_image=service_image)

        for vehicle_image in vehicle_images:
            VehicleImage.objects.create(vehicle=vehicle, vehicle_image=vehicle_image)
        return redirect('/')  

    return render(request, 'add-vehicle.html')

# ----------------------------------------------------------------------------------Edit Add Vehicle ---------------------------------------------------------

@login_required
def edit_add_vehicle_view(request, id):
    vehicle = Vehicle.objects.get(id=id)
    vehicle_image = VehicleImage.objects.filter(vehicle=id).last()
    service_record = ServiceRecord.objects.filter(vehicle=id).last()

    if request.method == 'POST':
        data_dict1 = request.POST.copy()
        data_dict1.update(request.FILES)

        serializer = EditAddVehicleSerializers(instance=vehicle, data=data_dict1, partial=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return redirect('home')
        else:
            return render(request, 'edit-add-vehicle.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'edit-add-vehicle.html', {'vehicle': vehicle, 'vehicle_image': vehicle_image, 'service_record': service_record})


# ---------------------------------------------------------------------------Delete Vehicle -----------------------------------------------------------
@login_required
def DeleteVehicleView(request, id):
    print(id, 'vehicle id')
    if id:
        Vehicle.objects.filter(id=id).update(status="Delete")
        return redirect('home')
    return render(request, 'home.html')


# ---------------------------------------------------------------------------------Add Future Vehicle -------------------------------------------------------------------------------
@login_required
def AddFutureVehicleView(request):
    if request.method == 'POST':
        data_dict1 = request.POST.copy()
        # data_dict1.update(request.FILES)
        print(data_dict1, '99')
        serializer = FutureVehicleSerializers(data=data_dict1)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            # time.sleep(5)
            return redirect('/')
            # return render(request, 'home.html')  
        else:
            return render(request, 'home.html', {'errors': serializer.errors})

    return render(request, 'add-future-vehicle.html')

# -------------------------------------------------------------------------------- Delete Future Vehicle -------------------------------------------------------------------------------

@login_required
def DeleteFutureVehicleView(request, id):
    print(id, 'vehicle id')
    if id:
        FutureVehicle.objects.filter(id=id).update(status="Delete")
        return redirect('home')
    return render(request, 'home.html')

# -------------------------------------------------------------------------------- Edit Future Vehicle -------------------------------------------------------------------------------

@login_required
def edit_future_vehicle_view(request, id):
    FutureVehi = FutureVehicle.objects.get(id=id)
    print(FutureVehi, '6666666666666666666666666666666666666666666666')

    if request.method == 'POST':
        data_dict1 = request.POST.copy()
        print(data_dict1, ' 66666666666666666666666666666666666666')
        data_dict1.update(request.FILES)
        print(data_dict1, '797979799797979799999999999999999999999999999999999999979')

        serializer = EditFutureVehicleSerializers(instance=FutureVehi, data=data_dict1, partial=True)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return redirect('home')
        else:
            return render(request, 'edit-future-vehicle.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'edit-future-vehicle.html', {'FutureVehi': FutureVehi})



# ---------------------------------------------------------------------------------Home page -------------------------------------------------------------------------------

def homeview(request):
    addvehicles = Vehicle.objects.filter(status = "Active").order_by('-id')
    addFutureVehicles = FutureVehicle.objects.filter(status = "Active").order_by('-id')

    if not addvehicles and not addFutureVehicles:
        message = "No vehicles available. Please add vehicles to the database."
        return render(request, 'home.html', {'message': message})

    return render(request, "home.html", {'addvehicles': addvehicles, 'addFutureVehicles': addFutureVehicles})

# --------------------------------------------------------------------Profile View ------------------------------------------------------------------
@login_required
def WishToSellView(request):

    if request.method == 'POST':
        asking_price = request.POST.get('asking_price')
        mileage_vehicle = request.POST.get('mileage_vehicle')
        
        data_dict = {
            'asking_price': asking_price,
            'mileage_vehicle': mileage_vehicle,
        }

        serializer = WishToSellSerializers(data=data_dict)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            time.sleep(5)

            return redirect('/')  
        else:
            return render(request, 'home.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'home.html')

# -------------------------------------------------------------------- Shop Insurance Page ------------------------------------------------------------------

@login_required
def shop_insurance_view(request, id=None):
    vehicle = Vehicle.objects.filter(id=id).last()

    if request.method == 'POST':
        serializer = ShopInsuranceSerializer(data=request.POST, instance=vehicle)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            time.sleep(5)
            return redirect('home')
        else:
            return render(request, 'home.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'home.html', {'vehicle': vehicle})

# -------------------------------------------------------------------- Purchase Page ------------------------------------------------------------------

@login_required
def Purchase_Vehicle_view(request, id=None):
    vehicle = Vehicle.objects.filter(id=id).last()

    if request.method == 'POST':
        serializer = PurchaseVehicleSerializers(data=request.POST, instance=vehicle)

        if serializer.is_valid(raise_exception=True):
            serializer.save()
            time.sleep(5)
            return redirect('home')
        else:
            return render(request, 'home.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

    return render(request, 'home.html', {'vehicle': vehicle})

# -------------------------------------------------------------------- FOrget password Page ------------------------------------------------------------------

def F_password_view(request):
    return render(request, "Forget_password.html")

# -------------------------------------------------------------------- Send OTP Page ------------------------------------------------------------------

def send_otp(request):
    error_message = None
    otp = random.randint(1111, 9999)
    email = request.POST.get('email')
    user = User.objects.filter(email=email).first()  

    if user:
        user.otp = otp
        user.save()

        # Send OTP via email
        email_subject = 'Your OTP'
        email_message = "Your One Time Password Is: " + str(otp)
        email_from = settings.DEFAULT_FROM_EMAIL
        email_to = [email]

        try:
            email = EmailMessage(email_subject, email_message, email_from, email_to)
            email.send()
            request.session['email'] = email_to[0]  
            return redirect('enter_otp')
        except Exception as e:
            error_message = 'Failed to send OTP via email'
    else:
        error_message = 'Invalid Email, please enter a valid email'

    return render(request, "Forget_password.html", {'error_message': error_message})

# -------------------------------------------------------------------- Enter Otp Page ------------------------------------------------------------------

def enter_otp(request):
    error_message = None
    email = request.session.get('email')

    if email:
        user = User.objects.filter(email=email).first()

        if user:
            user_otp = user.otp

            if request.method == 'POST':
                otp = request.POST.get('otp')

                if not otp:
                    error_message = "OTP is required"
                    print(otp, 'OTP is required')
                elif not user_otp == otp:
                    error_message = "OTP is invalid"
                    print(otp, 'OTP is invalid')
                if not error_message:
                    return redirect("password_reset")
                    # return render(request, 'New_password_set.html')

    else:
        return render(request, "Forget_password.html", {'error_message': 'Invalid email session'})

    return render(request, "enter_otp.html", {'error_message': error_message})

# -------------------------------------------------------------------- Password Reset Page ------------------------------------------------------------------

def password_reset(request):
    error_message = None
    email = request.session.get('email')

    if email:
        user = User.objects.filter(email=email).first()

        if request.method == 'POST':
            new_password = request.POST.get('new_password')
            confirm_new_password = request.POST.get('confirm_new_password')

            if not new_password:
                error_message = "New Password is required"
            elif not confirm_new_password:
                error_message = "Confirm New Password is required"
            elif new_password != confirm_new_password:
                error_message = "New Password and Confirm Password do not match"
            else:
                # Securely set and hash the new password
                user.set_password(new_password)
                user.save()

                messages.success(request, 'Password is changed successfully')
                return redirect('login')

        return render(request, "New_password_set.html", {'error_message': error_message})
    else:
        return render(request, "New_password_set.html", {'error_message': 'Invalid email session'})

# --------------------------------------------------------------------  Page ------------------------------------------------------------------


def garage_offer_view(request):
    return render(request, "garage-offer.html")


def home_one_view(request):
    return render(request, "home-one.html")


def home_two_view(request):
    return render(request, "home-two.html")


# def purchase_offer_view(request, vehicle_id):
#     vehicle = Vehicle.objects.get(pk=vehicle_id)
#     print(vehicle, 'nennnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnnn')
#     matching_details = vehicle.get_matching_future_vehicle()
#     print(matching_details, 'pppppppppppppppppppppppppppppppppppppppppp')

#     return render(request, 'purchase_offer.html', {'vehicle': vehicle, 'matching_details': matching_details})



# def AddVehicleView(request, format=None):
#     if request.method == 'POST':
#         data_dict = request.POST.copy()
#         data_dict.update(request.FILES)

#         service_dates = request.POST.getlist('service_date')
#         print(service_dates,'<>>>>>>>>>>>>>>>>>>>>>>>>>')
#         service_images = request.FILES.getlist('service_image')
#         print(service_images,'<<<<<<<<<<<<<<<<<<<>>>>>>>>>>>>>>>>>image')
#         vehicle_images = request.FILES.getlist('vehicle_image')
#         print(vehicle_images,'<<<<<<<<<<<<<<<<<<<<<<<<<<vehicle image>>>>>>>>>>>>>>>>>>')

#         # record_data = []

#         # Create a list of dictionaries with the corresponding data
#         # for date, service_image, vehicle_image in zip(service_dates, service_images, vehicle_images):
#         #     data_dict.update({
#         #         'service_date': date,
#         #         'service_image': service_image,
#         #         'vehicle_image': vehicle_image,
#         #     })

        
#         #     serializer = AddVehicleSerializer(data=data_dict)
#         #     if serializer.is_valid():
#         #         serializer.save()
#     return render(request, 'add-vehicle.html', status=405)



# -------------------------------------------------------------------- Shop Insurance Page ------------------------------------------------------------------

# @login_required
# def shop_insurance_view(request, id=None):
#     Vehicles = Vehicle.objects.filter(id=id).last()
#     print(Vehicles, '9999999999999999999999999999999999999')

#     if request.method == 'POST':
#         How_Much_you_Paying = request.POST.get('How_Much_you_Paying')
#         print(How_Much_you_Paying, '33333333333333333333333333333333333')
#         How_Much_want_Pay = request.POST.get('How_Much_want_Pay')
#         print(How_Much_want_Pay, '444444444444444444444444444444444444')
#         Current_Insurer = request.POST.get('Current_Insurer')
#         print(Current_Insurer, '555555555555555555555555555555555555')
#         all_Vehicles_listed = request.POST.get('all_Vehicles_listed')
#         print(all_Vehicles_listed, '666666666666666666666666666666666666')

#         data_dict1 = {
#             'Vehicles': Vehicles,
#             'How_Much_you_Paying': How_Much_you_Paying,
#             'How_Much_want_Pay': How_Much_want_Pay,
#             'Current_Insurer': Current_Insurer,
#             'all_Vehicles_listed': all_Vehicles_listed,
#         }
#         print(data_dict1, '77777777777777777777777777')

#         serializer = ShopInsuranceSerializer(instance=Vehicles, data=data_dict1, partial=True)
#         print(serializer, 'ssssssssssssssssssssssssssssssssssssssssssssssssssssssssss')

#         if serializer.is_valid(raise_exception=True):
#             print('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy')
#             serializer.save()
#             print(serializer.data, '8888888888888888888888888888888888888888888888888888888')

#             return redirect('home')
#         else:
#             return render(request, 'home.html', {'errors': serializer.errors}, status=status.HTTP_400_BAD_REQUEST)

#     return render(request, 'home.html', {'Vehicles': Vehicles})